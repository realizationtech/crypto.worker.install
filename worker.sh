#!/bin/bash
apt-get install screen

LocalUser=$(whoami)

sudo echo "[Unit]
Description=crypto.worker.service
After=network.target

[Service]
User=$(logname)
Restart=always
Type=simple
ExecStart=/opt/realization/crypto.worker.release/run.sh
WorkingDirectory=/opt/realization/crypto.worker.release/

[Install]
WantedBy=multi-user.target
" > /etc/systemd/system/crypto.worker.service

mkdir /opt/realization
#cp crypto.worker.service /etc/systemd/system/crypto.worker.service

cd /opt/realization
git clone https://bitbucket.org/realizationtech/crypto.worker.release.git
chmod +x /opt/realization/crypto.worker.release/run.sh
chown -R $(logname):$(logname) /opt/realization/*

systemctl daemon-reload
systemctl enable crypto.worker.service
systemctl start crypto.worker.service